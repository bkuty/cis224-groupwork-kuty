//Card array
var game_array = ['K','K','Q','Q','J','J','10','10','9','9','8','8','7','7','6','6','5','5','4','4','3','3','2','2'];
var game_values = [];
var game_card_ids = [];
var cards_flipped = 0;
//Shuffles the cards
Array.prototype.game_card_shuffle = function(){
    var i = this.length, j, temp;
    while(--i > 0){
        j = Math.floor(Math.random() * (i+1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
}
//Sets up the board
function setup(){
	cards_flipped = 0;
	var output = '';
    game_array.game_card_shuffle();
	for(var i = 0; i < game_array.length; i++){
		output += '<div id="card_'+i+'" onclick="gameFlipCard(this,\''+i+'\')"></div>';
	}
	document.getElementById('game_board').innerHTML = output;
}
function gameFlipCard(card,testval){
     
    val = game_array[testval];
    //Checks the cards value and then gives it the proper image
	if(card.innerHTML == "" && game_values.length < 2){
		//card.style.background = '#ffffff';
      
        if (val == 'K')
            {
              card.style.background = '#ffffff';
		      document.getElementById('card_'+testval).style.backgroundImage = 'url(images/king.jpg)';
            }
        else if (val == 'Q')
            {
              card.style.background = '#ffffff';
              document.getElementById('card_'+testval).style.backgroundImage = 'url(images/queen.jpg)';
            }
        else if (val == 'J')
            {
                card.style.background = '#ffffff';
                document.getElementById('card_'+testval).style.backgroundImage = 'url(images/jack.jpg)';
            }
        else if (val == '10')
            {
             // card.style.background = '#ffffff';
             document.getElementById('card_'+testval).style.backgroundImage = 'url(images/spade_10.png)';
		     // card.innerHTML = val;
            }
        else if (val == '9')
            {
              card.style.background = '#ffffff';
		      document.getElementById('card_'+testval).style.backgroundImage = 'url(images/spade_9.png)';
            }
        else if (val == '8')
            {
              card.style.background = '#ffffff';
		      document.getElementById('card_'+testval).style.backgroundImage = 'url(images/spade_8.png)';
            }
        else if (val == '7')
            {
              card.style.background = '#ffffff';
              document.getElementById('card_'+testval).style.backgroundImage = 'url(images/spade_7.png)';
            }
        else if (val == '6')
            {
              card.style.background = '#ffffff';
		      document.getElementById('card_'+testval).style.backgroundImage = 'url(images/spade_6.png)';
            }
        else if (val == '5')
            {
              card.style.background = '#ffffff';
		      document.getElementById('card_'+testval).style.backgroundImage = 'url(images/spade_5.png)';
            }
        else if (val == '4')
            {
              card.style.background = '#ffffff';
		      document.getElementById('card_'+testval).style.backgroundImage = 'url(images/spade_4.png)';
            }
        else if (val == '3')
            {
                  card.style.background = '#ffffff';
                  document.getElementById('card_'+testval).style.backgroundImage = 'url(images/spade_3.png)';
            }
        else if (val == '2')
            {
              card.style.background = '#ffffff';
		      document.getElementById('card_'+testval).style.backgroundImage = 'url(images/spade_2.png)';
            }
        // card.innerHTML ="";
          
        //Checks the values of the cards and if they match
		if(game_values.length == 0){
			game_values.push(val);
			game_card_ids.push(card.id);
            
		} else if(game_values.length == 1){
			game_values.push(val);
			game_card_ids.push(card.id);
            
			if(game_values[0] == game_values[1]){
				cards_flipped += 2;
				game_values = [];
            	game_card_ids = [];
                //Checks if all cards are flipped and then creates a new board.
				if(cards_flipped == game_array.length){
					alert("You win! Setting up a new board.");
					document.getElementById('game_board').innerHTML = "";
					setup();
				}
			} else {
                //Checks the flipped card values and if they dont match flips them back.
				function flip(){
				    
				    var card_1 = document.getElementById(game_card_ids[0]);
				    var card_2 = document.getElementById(game_card_ids[1]);
				    card_1.style = ('');
            	    card_1.innerHTML = "";
				    card_2.style = ('');
            	    card_2.innerHTML = "";
				    game_values = [];
            	    game_card_ids = [];
				}
				setTimeout(flip, 500);
			}
		}
	}
}